#include <OsqarSerialProtocol.h>
#include <StateMachine.h>
#include <Queu.h>

Sample TestSample;
Sample SampleArray[7];

RxBuffer Buffer;

float ActualTime=0;
float LastTime=0;
uint8_t TestData[SampleSize];

void setup()
{
  Buffer.Initialize();
  Serial.begin(57600);
  while(Serial.available()<=0)
  {
  }
  Buffer.Enqueu(5);
  Buffer.Enqueu(6);  
  Buffer.Enqueu(7);
  Buffer.Enqueu(8);    
  Buffer.Enqueu(9);
  Buffer.Enqueu(10);  
  Buffer.Enqueu(11);
  Buffer.Enqueu(12);  
  Serial.print("\nElements in buffer: ");
  Serial.print(Buffer.BytesInBuffer());  
  Serial.print("\nReadFirstElement: ");
  Serial.print(Buffer.ReadFirstElement());  
  Serial.print("\nReadLastElement: ");
  Serial.print(Buffer.ReadLastElement());
  
  Serial.print("\n\nDequeuing");  
  Serial.print("\nDequeed element: ");
  Serial.print(Buffer.Dequeu());
  Serial.print("\n\nElements in buffer: ");
  Serial.print(Buffer.BytesInBuffer());  
  Serial.print("\nReadFirstElement: ");
  Serial.print(Buffer.ReadFirstElement());  
  Serial.print("\nReadLastElement: ");
  Serial.print(Buffer.ReadLastElement()); 
 
  Serial.print("\n\nDequeuing");  
  Serial.print("\nDequeed element: ");
  Serial.print(Buffer.Dequeu());
  Serial.print("\n\nElements in buffer: ");
  Serial.print(Buffer.BytesInBuffer());  
  Serial.print("\nReadFirstElement: ");
  Serial.print(Buffer.ReadFirstElement());  
  Serial.print("\nReadLastElement: ");
  Serial.print(Buffer.ReadLastElement()); 

  Serial.print("\n\nDequeuing");  
  Serial.print("\nDequeed element: ");
  Serial.print(Buffer.Dequeu());
  Serial.print("\n\nElements in buffer: ");
  Serial.print(Buffer.BytesInBuffer());  
  Serial.print("\nReadFirstElement: ");
  Serial.print(Buffer.ReadFirstElement());  
  Serial.print("\nReadLastElement: ");
  Serial.print(Buffer.ReadLastElement()); 

  Serial.print("\n\nDequeuing");  
  Serial.print("\nDequeed element: ");
  Serial.print(Buffer.Dequeu());
  Serial.print("\n\nElements in buffer: ");
  Serial.print(Buffer.BytesInBuffer());  
  Serial.print("\nReadFirstElement: ");
  Serial.print(Buffer.ReadFirstElement());  
  Serial.print("\nReadLastElement: ");
  Serial.print(Buffer.ReadLastElement()); 

  Serial.print("\n\nDequeuing");  
  Serial.print("\nDequeed element: ");
  Serial.print(Buffer.Dequeu());
  Serial.print("\n\nElements in buffer: ");
  Serial.print(Buffer.BytesInBuffer());  
  Serial.print("\nReadFirstElement: ");
  Serial.print(Buffer.ReadFirstElement());  
  Serial.print("\nReadLastElement: ");
  Serial.print(Buffer.ReadLastElement()); 

  Serial.print("\n\nDequeuing");  
  Serial.print("\nDequeed element: ");
  Serial.print(Buffer.Dequeu());
  Serial.print("\n\nElements in buffer: ");
  Serial.print(Buffer.BytesInBuffer());  
  Serial.print("\nReadFirstElement: ");
  Serial.print(Buffer.ReadFirstElement());  
  Serial.print("\nReadLastElement: ");
  Serial.print(Buffer.ReadLastElement());   
  
  Serial.print("\n\nDequeuing");  
  Serial.print("\nDequeed element: ");
  Serial.print(Buffer.Dequeu());
  Serial.print("\n\nElements in buffer: ");
  Serial.print(Buffer.BytesInBuffer());  
  Serial.print("\nReadFirstElement: ");
  Serial.print(Buffer.ReadFirstElement());  
  Serial.print("\nReadLastElement: ");
  Serial.print(Buffer.ReadLastElement()); 

  Serial.print("\n\nDequeuing");  
  Serial.print("\nDequeed element: ");
  Serial.print(Buffer.Dequeu());
  Serial.print("\n\nElements in buffer: ");
  Serial.print(Buffer.BytesInBuffer());  
  Serial.print("\nReadFirstElement: ");
  Serial.print(Buffer.ReadFirstElement());  
  Serial.print("\nReadLastElement: ");
  Serial.print(Buffer.ReadLastElement()); 

  Serial.print("\n\nDequeuing");  
  Serial.print("\nDequeed element: ");
  Serial.print(Buffer.Dequeu());
  Serial.print("\n\nElements in buffer: ");
  Serial.print(Buffer.BytesInBuffer());  
  Serial.print("\nReadFirstElement: ");
  Serial.print(Buffer.ReadFirstElement());  
  Serial.print("\nReadLastElement: ");
  Serial.print(Buffer.ReadLastElement());     
  
  Serial.print("\n\nDequeuing");  
  Serial.print("\nDequeed element: ");
  Serial.print(Buffer.Dequeu());
  Serial.print("\n\nElements in buffer: ");
  Serial.print(Buffer.BytesInBuffer());  
  Serial.print("\nReadFirstElement: ");
  Serial.print(Buffer.ReadFirstElement());  
  Serial.print("\nReadLastElement: ");
  Serial.print(Buffer.ReadLastElement());      
}

void loop()
{
}
