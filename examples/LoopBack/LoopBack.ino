#include <OsqarSerialProtocol.h>
#include <StateMachine.h>
#include <Queu.h>

Sample TestSample;
Sample FeedBackSample;
Sample SampleArray[7];

TxManager Tx;
RxManager Rx;

float ActualTime=0;
float LastTime=0;
uint8_t TestData[SampleSize];
uint8_t RecievedDataArray[SampleSize];

void setup()
{
  FeedBackSample.Initialize();
  TestSample.Initialize();
  Tx.Initialize();
  Rx.Initialize();
  for(int i=0;i<7;i++)
  { 
    SampleArray[i].Initialize();
  }
  Serial.begin(57600);
  while(Serial.available()<=0)
  {
  }
  
  for(int i=0;i<SampleSize;i++)
  {
    TestData[i]=10*i+20;
    RecievedDataArray[i]=TestData[i];
    TestSample.Add(&TestData[i]);
    FeedBackSample.Add(&RecievedDataArray[i]);    
  }

  Tx.SubscribeSample(&FeedBackSample);
}

void loop()
{
    Rx.Runner();
    if(Rx.NewUnreadData())
    {
      //delay(2000);
      for(int i=0;i<SampleSize;i++)
      {
        RecievedDataArray[i]=Rx.ReadByte(i);
      }
    }
    Tx.Runner();
}
