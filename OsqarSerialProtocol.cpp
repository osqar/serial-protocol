#include "OsqarSerialProtocol.h"

TxManager * PointerToTx;
RxManager * PointerToRx;

/***********************************************************
						CLASS RxBuffer
************************************************************/
void RxBuffer::Initialize()
{
	Pointer=0; //empty
	for(int i=0;i<RxBufferSize;i++)
	{
		Buffer[i]=0;
	}
}

void RxBuffer::Enqueu(uint8_t Byte)
{
	bool Full=0;
	Full=this->isBufferFull();
	if(!Full)//if there is room for another byte to queu
	{
		this->EnqueuProcedure(Byte);
	}
}

uint8_t RxBuffer::Dequeu()
{
	bool Empty=0;
	uint8_t Byte=0; //0
	
	Empty=this->isBufferEmpty();
	if(!Empty)//if not empty, then Dequeu new value
	{
		Byte=this->DequeuProcedure();
	}
	return Byte;
}

uint8_t RxBuffer::ReadLastElement()
{
	bool Empty=0;
	uint8_t Index=0;//initializing
	
	Empty=this->isBufferEmpty();
	if(!Empty)//if Buffer isn't empty
	{
		Index=Pointer-1; //Update index value
		return Buffer[Index]; //return last pointed value
	}
	else
		return 0; //0
}

uint8_t RxBuffer::ReadFirstElement()
{
	bool Empty=0;

	Empty=this->isBufferEmpty();
	if(!Empty)//if Buffer isn't empty
	{
		return Buffer[0]; //return First element in queu
	}
	else
		return 0; //0
}

uint8_t RxBuffer::BytesInBuffer()
{
	return Pointer; //is the equivalent of number of bytes in buffer
}

bool RxBuffer::isBufferFull()
{
	int Remaining=0;
	Remaining=RxBufferSize-Pointer;
	
	if(Remaining==0)
		return 1; //is full
	else
		return 0; //not full
}

bool RxBuffer::isBufferEmpty()
{
	if(Pointer==0)
		return 1; //is empty
	else
		return 0; //not empty	
}

void RxBuffer::EnqueuProcedure(uint8_t Byte)
{
	bool Full=0;
	Full=this->isBufferFull();
	if(!Full) //safetly Enqueu if not full
	{
		Pointer++; //increase size
		uint8_t Index=0;
		Index=Pointer-1;
		
		Buffer[Index]=Byte; //update value
	}
}

uint8_t RxBuffer::DequeuProcedure()
{
	bool Empty=0;
	Empty=this->isBufferEmpty();
	
	if(!Empty)//safetly Dequeu if not empty
	{
		uint8_t Byte=0;
		Byte=this->ReadFirstElement(); //dequeu
		
		uint8_t ElementsInBuffer=0;
		ElementsInBuffer=this->BytesInBuffer();
		
		for(int i=1;i<ElementsInBuffer;i++) //re-arrange elements in queu
		{
			Buffer[i-1]=Buffer[i];
		}
		Pointer=Pointer-1; //update Buffer size
		
		return Byte;
	}
	else
		return 0; //return 0
}

/***********************************************************
						CLASS RxManager
************************************************************/

void RxManager::Initialize()
{
	//STRX Flags
	HeaderDetected=0; //For STM
	FinishedCount=0; //For STM
	ACKRecievedFlag=0; //For STM
	FinishedRecieving=0; //for user
	NewDataInBuffer=0; //for STM
	
	ID=0;
	LowData=0;
	HighData=0;
	PointerToRx=this;
	SampleRate=DefaultRxRate;
	ActualTime=0;
	LastTime=0;
	SOHCount=0;
	ByteCount=0;
	Buffer.Initialize();
	
	//initialize Arrays
	for(int i=0;i<SampleSize+2;i++)
	{
		DataArray[i]=0;
		DataOut[i]=0;
	}
	
	//Initialize States
	
	//First State
	DoWhenEnter=this->Initializing;
	Do=this->DetectingID;
	DoWhenOut=this->SetIDFlag;
	DetectingHeader.Create(&DetectingHeader,&GatheringBytes,Do,DoWhenEnter,DoWhenOut);
	//Second State
	DoWhenEnter=this->SetByteCount;
	Do=this->MappingAndAdding;
	DoWhenOut=this->SetFCFlag;
	GatheringBytes.Create(&VerifyingData,Do,DoWhenEnter,DoWhenOut);
	//ThirdState
	//This state does nothing, but it could do CheckSum, CRC or something related
	DoWhenEnter=this->Default;
	Do=this->MatchIDWithEOH;
	DoWhenOut=this->Default;
	VerifyingData.Create(&RefreshValues,&DetectingHeader,Do,DoWhenEnter,DoWhenOut);
	//FourthState
	DoWhenEnter=this->Default;
	Do=this->UpdatingDataOut;
	DoWhenOut=this->SetFRFlag;
	RefreshValues.Create(&DetectingHeader,Do,DoWhenEnter,DoWhenOut);
	
	//Initialize STRX
	STRX.Initialize(&DetectingHeader);
}

uint8_t RxManager::ReadID()
{
	FinishedRecieving=0;
	uint8_t ReturnData=0;
	ReturnData=ID;
	ID=0;
	return ReturnData;
}

long RxManager::ReadLowData()
{
	FinishedRecieving=0;
	long ReturnData=0;
	ReturnData=LowData;
	LowData=0;
	return ReturnData;
}

long RxManager::ReadHighData()
{
	FinishedRecieving=0;
	long ReturnData=0;
	ReturnData=HighData;
	HighData=0;
	return ReturnData;
}

uint8_t RxManager::ReadByte(uint8_t Index)
{
	FinishedRecieving=0;
	if(Index>=SampleSize)
		Index=SampleSize-1;
	uint8_t ReturnData=0;
	ReturnData=DataOut[Index];
	DataOut[Index]=0;
	return ReturnData;
}

bool RxManager::isACKRecieved()
{
	return 1;
}

void RxManager::Runner()
{
	ActualTime=millis();
	if(ActualTime-LastTime>=SampleRate)
	{
		LastTime=millis();
		while(Serial.available()>0)
		{
			Buffer.Enqueu(Serial.read());	//release Serial Buffer	
		}
	}
	bool BufferEmpty=0;
	BufferEmpty=Buffer.isBufferEmpty();
	if(!BufferEmpty) //if not empty //for not dequeuing 0's when is not supposed to
	{
		RecievedByte=Buffer.Dequeu();		
		//Set new recieved data flag
		NewDataInBuffer=1;
	}
	//Attend and process RecievedData
	STRX.Runner();	
}

void RxManager::SetRxRate(long Rate)
{
	if(Rate>MinRxRate)
		Rate=MinRxRate;
	if(Rate<MaxTxRate)
		Rate=MaxRxRate;
	SampleRate=Rate;
}

bool RxManager::NewUnreadData()
{
	return FinishedRecieving;
}

//STRX Functions

//DetectingHeader Methods
uint8_t RxManager::Initializing()
{
	PointerToRx->HeaderDetected=0;
	PointerToRx->FinishedCount=0;
	PointerToRx->ACKRecievedFlag=0;
	PointerToRx->SOHCount=0;
	PointerToRx->ByteCount=0;
	
	return 0;
}

uint8_t RxManager::DetectingID()
{
	if(PointerToRx->NewDataInBuffer)
	{
		PointerToRx->NewDataInBuffer=0;//clear flag
		
		if(PointerToRx->RecievedByte==SOH)
		{
			PointerToRx->SOHCount++;
			if(PointerToRx->SOHCount>4) //invalid sample
			{
				return 1; //restart count
			}
			return 0;	//keep counting
		}
		else
		{
			if(PointerToRx->RecievedByte==0)
			{
				if(PointerToRx->SOHCount>=HeaderRedundance)
				{
					PointerToRx->HeaderDetected=1;			
					return 2; //next state
				}
			}
		}
		return 1;	//restart count
	}
}

uint8_t RxManager::SetIDFlag()
{
	//if Header detected, ID flag is set
	PointerToRx->FinishedCount=0;
	PointerToRx->ACKRecievedFlag=0;
	PointerToRx->SOHCount=0;
	PointerToRx->ByteCount=0;
	
	return 0;
}

//GatheringBytes Methods
uint8_t RxManager::SetByteCount()
{
	PointerToRx->HeaderDetected=1;
	PointerToRx->FinishedCount=0;
	PointerToRx->ACKRecievedFlag=0;
	PointerToRx->SOHCount=0;
	PointerToRx->ByteCount=0;
	
	return 0;
}

uint8_t RxManager::MappingAndAdding()
{
	if(PointerToRx->NewDataInBuffer) //if new data
	{
		PointerToRx->NewDataInBuffer=0; //clear flag
		
		uint8_t Index=0;
		int8_t AuxiliarRecieved=0;
		
		//Mapping
		AuxiliarRecieved=(int8_t)PointerToRx->RecievedByte;
		//if(PointerToRx->ByteCount>0)//ID isn't coded
			//AuxiliarRecieved=ceil(map(AuxiliarRecieved,-100,100,-128,127));	
		
		//Adding
		Index=PointerToRx->ByteCount;
		PointerToRx->DataArray[Index]=(uint8_t)AuxiliarRecieved;
		PointerToRx->ByteCount++;
	}
		if(PointerToRx->ByteCount>=SampleSize+2)
		{
			return 1;
		}
		return 0;	
}

uint8_t RxManager::SetFCFlag()
{
	PointerToRx->HeaderDetected=1;
	PointerToRx->FinishedCount=1;
	PointerToRx->ACKRecievedFlag=0;
	PointerToRx->SOHCount=0;
	PointerToRx->ByteCount=0;
	
	return 0;	
}

//VerufyingData Methods
uint8_t RxManager::Default()
{
	return 0;
}

uint8_t RxManager::MatchIDWithEOH()
{
	uint8_t RecievedEOH=0;
	uint8_t RecievedID=0;
	
	RecievedID=PointerToRx->DataArray[0];
	RecievedEOH=PointerToRx->DataArray[SampleSize+1];
	
	if(RecievedEOH-EOH==RecievedID)//valid Sample (ID CheckSum)
	{
		bool T=1;
	}
	return 1; //allways goes to next state
}

//UpdatingDataOut Methods
uint8_t RxManager::UpdatingDataOut()
{
	PointerToRx->ID=PointerToRx->DataArray[0]; //Update ID Value
	
	//Refreshing values to be read
	for(int i=0;i<SampleSize;i++)
	{		
		uint8_t ArrayIndex=i+1;
		uint8_t OutIndex=i;
		
		PointerToRx->DataOut[OutIndex]=PointerToRx->DataArray[ArrayIndex];
		
		//if(SampleSize==8) //That's the only size that Low and High Data may be valid
		//{
			long Mask=0;
			Mask=PointerToRx->DataOut[OutIndex];		
			if(i<4) //HighData
			{
				PointerToRx->HighData=PointerToRx->HighData|(Mask<<(24-(8*i)));				
			}
			else //LowData
			{
				PointerToRx->LowData=PointerToRx->LowData|(Mask<<(56-(8*i)));
			}
		//}
	}
	return 1;
}

uint8_t RxManager::SetFRFlag()
{
	PointerToRx->HeaderDetected=0;
	PointerToRx->FinishedCount=0;
	PointerToRx->ACKRecievedFlag=0;
	PointerToRx->FinishedRecieving=1;
	PointerToRx->SOHCount=0;
	PointerToRx->ByteCount=0;
	return 0;
}
/***********************************************************
						CLASS TxManager
************************************************************/

void TxManager::Initialize()
{
	//Empty
	for(int i=0;i<TxBufferSize;i++)
	{
		SampleBuffer[i]=0;
		Spots[i]=0;
	}
	SampleCount=0;
	
	//only modified by state machine methods
	PointerToData=0;
	PointerToSample=0;
	PointerToTx=this;
	FinishedCount=0;
	SampleRate=DefaultTxRate;
	ActualTime=0;
	LastTime=0;
	
	
	//Initializing first state
	DoWhenEnter=this->UpdateSamplePointer;
	Do=this->GetNewSample;
	DoWhenOut=this->Default;
	GettingNewSample.Create(&CodifyingSample,Do,DoWhenEnter,DoWhenOut);
	
	//Initializing second state
	DoWhenEnter=this->SetDataPointer;
	Do=this->EncodeData;
	DoWhenOut=this->UpdateDataPointer;
	CodifyingSample.Create(&CodifyingSample,&Sending,Do,DoWhenEnter,DoWhenOut);
	
	//Initializing third state
	DoWhenEnter=this->Default;
	Do=this->SendIfNoACK;
	DoWhenOut=this->Default;
	Sending.Create(&Sending,&GettingNewSample,Do,DoWhenEnter,DoWhenOut);
	
	STTX.Initialize(&GettingNewSample);
	
	Serial.begin(BaudRate);
}

void TxManager::SetTxRate(long Rate)
{
	if(Rate>MinTxRate)
		Rate=MinTxRate;
	if(Rate<MaxTxRate)
		Rate=MaxTxRate;
	SampleRate=Rate;
}

void TxManager::SubscribeSample(Sample * SubscriberSample)
{
	if(!this->isTxFull())
	{
		uint8_t Pointer=0;
		Pointer=this->GetAvailableSpot();
		this->SetSample(Pointer,SubscriberSample);
	}
}

void TxManager::UnsubscribeSample(Sample * SubscriberSample)
{
	if(this->isSampleSubscribed(SubscriberSample))
	{
		uint8_t Index=0;
		Index=SubscriberSample->GetID();
		Index=Index-1;
		this->ReleaseSpot(Index);
		SampleBuffer[Index]->SetID(0);//No valid ID
		SampleBuffer[Index]=0; //Pointing to Null
	}
}

uint8_t TxManager::GetAvailableSpot()
{
	if(!this->isTxFull()) //if any spot available
	{
		for(int i=0;i<TxBufferSize;i++)
		{
			if(this->isSpotAvailable(i))
				return i;
		}
	}
	return -1;
}

void TxManager::SetSample(uint8_t Index,Sample * SubscriberSample)
{
	if(Index<TxBufferSize)
	{
		if(this->isSpotAvailable(Index))
		{
			uint8_t ID=0;
			ID=Index+1;		
			SampleBuffer[Index]=SubscriberSample;//Refresh buffer value
			SampleBuffer[Index]->SetID(ID); //SetID to subscriber
			this->UseSpot(Index);
		}
	}
}

void TxManager::UseSpot(uint8_t Index)
{
	if(Index<TxBufferSize)
	{
		if(this->isSpotAvailable(Index))
		{		
			Spots[Index]=1; //set to Occupied			
			SampleCount++;
		}
	}
}

void TxManager::ReleaseSpot(uint8_t Index)
{
	if(Index<TxBufferSize)//valid index
	{
		if(!this->isSpotAvailable(Index))//Occupied
		{			
			Spots[Index]=0; //Set to available			
			SampleCount=SampleCount-1;
		}
	}
}

bool TxManager::isTxFull()
{
	if(SampleCount<TxBufferSize)
		return 0;
	else
		return 1;
}

bool TxManager::isSpotAvailable(uint8_t Index)
{
	if(Index<TxBufferSize)
	{
		if(!Spots[Index])//avaliable spot
			return 1;
	}
	return 0;
}

bool TxManager::isSampleSubscribed(Sample * SubscriberSample)
{
	uint8_t Index=0;
	Index=SubscriberSample->GetID();
	Index=Index-1;
	
	if(Index<TxBufferSize)
	{
		if(!this->isSpotAvailable(Index)) //occupied
			return 1;
	}
	return 0;
}

uint8_t TxManager::HowManyAdded()
{
	return SampleCount;
}

uint8_t TxManager::HowManyAvailable()
{
	return TxBufferSize-SampleCount;
}

void TxManager::Runner()
{
/* 	uint8_t SamplesAdded=0;
	SamplesAdded=this->HowManyAdded(); */
	ActualTime=millis();
	
	if(ActualTime-LastTime>=SampleRate)
	{
		LastTime=millis();
		for(int i=0;i<TxBufferSize;i++)
		{
			this->SendSample(i);			
		}
	}
	//STTX.Runner();
}

void TxManager::SendSample(uint8_t Index)
{
	if(Index<TxBufferSize)//valid Index
	{
		if(!this->isSpotAvailable(Index))//occupied
		{
 			/*Serial.print("\nSending Sample\n\n\n");
			Serial.print(SampleBuffer[Index]->GetID());
			Serial.print("\n\n\n"); */
			for(int i=0;i<HeaderRedundance;i++)
			{
				//Serial.print("\nHeader: ");
				Serial.write(SOH);
			}
			//Serial.print("\nEnd of Header: ");
			Serial.write(0);
			//Serial.print("\nID: ");
			Serial.write(SampleBuffer[Index]->GetID());
			
			for(int i=0;i<SampleSize;i++)
			{
				//Serial.print("\nData: ");
				uint8_t SampleToSend=0;
				SampleToSend=SampleBuffer[Index]->ReadData(i);
				if(SampleToSend==SOH)
					SampleToSend=SOH+1;
				Serial.write(SampleToSend);
			}
			
			//This is for CHECK ID in reciever
			Serial.write(EOH+SampleBuffer[Index]->GetID());
		}
	}
}

//GettingNewSample Methods
uint8_t TxManager::UpdateSamplePointer()
{
	uint8_t SamplesInBuffer=0;
	SamplesInBuffer=PointerToTx->HowManyAdded();
	
	PointerToTx->PointerToSample++;
	if(PointerToTx->PointerToSample==SamplesInBuffer) //Restart
	{
		PointerToTx->PointerToSample=0;
	}
	return 0;
}

uint8_t TxManager::GetNewSample()
{
	//Will send SampleBuffer[PointerToSample]
	uint8_t Index=0;
	Index=PointerToTx->PointerToSample;
	
	if(Index<TxBufferSize) //valid Index
	{
		if(!PointerToTx->isSpotAvailable(Index)) //occupied
		{
			PointerToTx->PointerToData=0; //initializes to 0
			return 1; //go next state
		}
	}
	return 0; //for for sample to be added
}

uint8_t TxManager::Default()
{
	return 0;
}

//CodifyingSample Methods
uint8_t TxManager::SetDataPointer()
{
	uint8_t Index=0;
	Index=PointerToTx->PointerToData;
	
	if(Index>=SampleSize) //invalid Index
	{
		PointerToTx->PointerToData=0; //restart
		PointerToTx->FinishedCount=1; //Set FC flag
	}	
	return 0;
}

uint8_t TxManager::EncodeData()
{
	//map to protocol range
	//numbers from -128 : 127 are mapped into -100 : 100
	//Treats bytes as integers //could be better to use Gray code
	if(!PointerToTx->FinishedCount)
	{
		uint8_t SampleIndex=0;
		SampleIndex=PointerToTx->PointerToSample;//Point to Sample
	
		uint8_t DataIndex=0;
		DataIndex=PointerToTx->PointerToData;//Point to Data
	
		//Encoding Sample's byte to byte
		PointerToTx->SendingSample[DataIndex]=0;//Initialize
		int8_t CodedData=0;
		CodedData=PointerToTx->SampleBuffer[SampleIndex]->ReadData(DataIndex);
		if(CodedData==128) //
		{
			CodedData=254;
		}
		//CodedData=map(CodedData,-128,127,-100,100);
		
		PointerToTx->SendingSample[DataIndex]=CodedData;
		return 1; //Continue Encoding
	}
	else
	{
		PointerToTx->FinishedCount=0; //Clear FC flag
		return 2;
	}
}

uint8_t TxManager::UpdateDataPointer()
{
	PointerToTx->PointerToData++;
	return 0;
}

//Sending Methods
uint8_t TxManager::SendIfNoACK()
{
	uint8_t Index=0;
	Index=PointerToTx->PointerToSample;
	PointerToTx->ActualTime=millis();
	
	if(PointerToTx->ActualTime-PointerToTx->LastTime>PointerToTx->SampleRate)
	{
		PointerToTx->LastTime=millis();
		if(Index<TxBufferSize)//valid Index
		{
			if(!PointerToTx->isSpotAvailable(Index))//occupied
			{
				for(int i=0;i<HeaderRedundance;i++)
				{
					//Sending Start Of Header;
					Serial.write(SOH);
				}
				//Sending EndOFHeader
				Serial.write(0);
				//Sending ID
				Serial.write(PointerToTx->SampleBuffer[Index]->GetID());
			
				//Sending Sample
				for(int i=0;i<SampleSize;i++)
				{
					//Coded Value
					Serial.write(PointerToTx->SendingSample[i]);//Serial.write(PointerToTx->SampleBuffer[Index]->ReadData(i));//
				}
			
				//This is for CHECK ID in reciever
				Serial.write(EOH+PointerToTx->SampleBuffer[Index]->GetID());
			}
		}
	}
	return 2; //to next sample
}

/***********************************************************
						CLASS Sample
************************************************************/

void Sample::Initialize()
{
	//Empty Sample
	ID=0;
	ByteCount=0;
	for(int i=0;i<SampleSize;i++)
	{
		Data[i]=0; //to null
		Spots[i]=0;
	}
}

//Writes in sequence
void Sample::Add(uint8_t * DataByte)
{
	if(!this->isSampleFull())
	{
		uint8_t Pointer=0;
		Pointer=this->FirstAvailable();
		this->WriteData(Pointer,DataByte);
	}
}

//Deletes Data index reference
bool Sample::Delete(uint8_t Index)
{
	if(Index<SampleSize)//make sure Index is valid
	{
		if(!this->isSpotAvailable(Index)) //if Occupied
		{
			Data[Index]=0;//to null
			this->ReleaseSpot(Index);
			return 1;
		}
	}
	return 0;
}

//Reads ID
uint8_t Sample::GetID()
{
	return ID;
}

//Writes ID
void Sample::SetID(uint8_t WrittenID)
{
	ID=WrittenID;
}

//Reads Indexed Data
uint8_t Sample::ReadData(uint8_t Index)
{
	if(Index<SampleSize)
		return *Data[Index];
	else
		return 0;
}

//Writes Data in Index
void Sample::WriteData(uint8_t Index, uint8_t * DataByte)
{
	if(this->isSpotAvailable(Index))
	{
		this->UseSpot(Index);	
		Data[Index]=DataByte;
	}
}

//Set Spot status to Occupied
void Sample::UseSpot(uint8_t Index)
{
	if(this->isSpotAvailable(Index))
	{
		Spots[Index]=1; //Set to occupied
		ByteCount++;
	}
}

//Set Spot status to available
void Sample::ReleaseSpot(uint8_t Index)
{
	if(Index<SampleSize)
	{
		if(!this->isSpotAvailable(Index))
		{
			Spots[Index]=0;
			ByteCount=ByteCount-1;
		}
	}
}

//Returns true if spot is available
bool Sample::isSpotAvailable(uint8_t Index)
{
	if(Index<SampleSize)
	{
		if(!Spots[Index])
			return 1;
	}
	return 0;
}

//Returns true if Spots are full
bool Sample::isSpotFull()
{
	if(ByteCount<SampleSize)
		return 0;
	else
		return 1;
}

//Return number of used Spots
uint8_t Sample::HowManyAdded()
{
	return ByteCount;
}

//Return number of available spots
uint8_t Sample::HowManyAvailable()
{
	return SampleSize-ByteCount;
}

//Returns index of the first available Spot
uint8_t Sample::FirstAvailable()
{
	if(!this->isSpotFull())//if any available
	{
		for(int i=0;i<SampleSize;i++)
		{
			if(this->isSpotAvailable(i))//if available
				return i;
		}
	}
	return -1;
}

//Return number of available Spots
bool Sample::isSampleFull()
{
	bool isFull;
	isFull=this->isSpotFull();

	return isFull;
}


