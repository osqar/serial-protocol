#ifndef OsqarSerialProtocol_h
#define OsqarSerialProtocol_h

#include <StateMachine.h>
#include <Queu.h>
#include <Arduino.h>
#include <inttypes.h>

#define BaudRate 57600
#define RxBufferSize 30
#define DefaultTxRate 400
#define DefaultRxRate 50
#define MinTxRate 2000
#define MaxTxRate 50
#define MinRxRate 1000
#define MaxRxRate 25
#define SOH 128
#define EOH 130	//It'll send and recieve EOH+ID
#define ACK 129
#define HeaderRedundance 4
#define TxBufferSize 8 //Samples
#define SampleSize 8 //bytes
#define True 1
#define False 0

class RxManager;
class RxBuffer;
class TxManager;
class Sample;

//could be using New and Delete operators
class RxBuffer
{
	public:
		void Initialize();
		void Enqueu(uint8_t);
		uint8_t Dequeu();
		uint8_t ReadLastElement();
		uint8_t ReadFirstElement();
		uint8_t BytesInBuffer();
		bool isBufferFull();		
		bool isBufferEmpty();
		void EnqueuProcedure(uint8_t);
		uint8_t DequeuProcedure();
	private:
		uint8_t Buffer[RxBufferSize];
		uint8_t Pointer;
};

class RxManager
{
	public:
		void Initialize();
		uint8_t ReadID();
		long ReadLowData();
		long ReadHighData();
		uint8_t ReadByte(uint8_t);
		bool isACKRecieved();
		void Runner();
		void SetRxRate(long);		
		bool NewUnreadData();
	private:
		float SampleRate;
		uint8_t DataArray[SampleSize+2]; //ID+Sample+EOH
		uint8_t DataOut[SampleSize+1]; //ID+Sample
		long LowData;
		long HighData;
		uint8_t SOHCount;
		uint8_t ByteCount;
		uint8_t RecievedByte;
		uint8_t ID;
		float ActualTime;
		float LastTime;
		RxBuffer Buffer;
	
		//StateMachine
		StateMachine STRX;
		State DetectingHeader;
		State GatheringBytes;
		State VerifyingData;
		State RefreshValues;
		
		//State Methods
		//DetectingHeader Methods
		static uint8_t Initializing();
		static uint8_t DetectingID();
		static uint8_t SetIDFlag();
		//GatheringBytes Methods
		static uint8_t SetByteCount();
		static uint8_t MappingAndAdding();
		static uint8_t SetFCFlag();
		//VerifyingData
		static uint8_t Default();
		static uint8_t MatchIDWithEOH();
		//RefreshValues
		static uint8_t UpdatingDataOut();
		static uint8_t SetFRFlag();
				
		//Values and flags
		bool HeaderDetected;
		bool FinishedCount;
		bool ACKRecievedFlag;
		bool FinishedRecieving;
		bool NewDataInBuffer;
		
		PointerToFunction Do;
		PointerToFunction DoWhenEnter;
		PointerToFunction DoWhenOut;
};

class TxManager
{
	public:	
		void Initialize();
		void SubscribeSample(Sample *);
		void UnsubscribeSample(Sample *);
				
		//write methods		
		void SetSample(uint8_t,Sample *);		
		
		uint8_t GetAvailableSpot();		
		void UseSpot(uint8_t);
		void ReleaseSpot(uint8_t);
		
		bool isTxFull();
		bool isSpotAvailable(uint8_t);
		bool isSampleSubscribed(Sample *);
		uint8_t HowManyAdded();
		uint8_t HowManyAvailable();
		
		//Sends all subscribed samples
		void Runner();
		void SendSample(uint8_t);
		void SetTxRate(long);
		
	private:
		uint8_t SampleCount;
		bool Spots[TxBufferSize];
		Sample * SampleBuffer[TxBufferSize];
		float SampleRate;
		float ActualTime;
		float LastTime;
		
		//StateMachine
		StateMachine STTX;
		State GettingNewSample;
		State CodifyingSample;
		State Sending;
		
		//State functions//
		
		//GettingNewSampleStateFunctions
		static uint8_t UpdateSamplePointer();
		static uint8_t GetNewSample();
		static uint8_t Default();
		//CodifyingSampleStateFunctions
		static uint8_t SetDataPointer();		
		static uint8_t EncodeData();
		static uint8_t UpdateDataPointer();
		//SendingStateFunctions
		static uint8_t SendIfNoACK();		
		
		uint8_t PointerToData;
		uint8_t PointerToSample;
		bool FinishedCount;
		uint8_t SendingSample[SampleSize];		
		
		PointerToFunction DoWhenEnter;
		PointerToFunction Do;
		PointerToFunction DoWhenOut;
};

class Sample
{
	public:
		void Initialize();
		void Add(uint8_t *);
		bool Delete(uint8_t);
		
		//read/write methods
		uint8_t GetID();
		void SetID(uint8_t);
		
		uint8_t ReadData(uint8_t);
		void WriteData(uint8_t,uint8_t *);
		
		void UseSpot(uint8_t);
		void ReleaseSpot(uint8_t);
		
		uint8_t HowManyAdded();
		uint8_t HowManyAvailable();
		uint8_t FirstAvailable();
		
		bool isSampleFull();
		bool isSpotAvailable(uint8_t);
		bool isSpotFull();
		
	private:
		//Gets ID when subscribes to TxManager
		uint8_t ID;		
		uint8_t * Data[SampleSize];
		
		//Every time it
		byte ByteCount;
		bool Spots[SampleSize];
};


#endif


